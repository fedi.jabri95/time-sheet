FROM maven:3.8.1-openjdk-8-slim
EXPOSE 8090
ADD target target
ENTRYPOINT ["java","-jar","target/timesheet.war"]
